# Calculatrice dockerisée



## Installation

1) Lancer le front avec :
	```bash
	sudo docker-compose -f ./front/docker-compose.yml up --build
	```
   L'adresse IP est définie statiquement dans docker-compose.yml, il faut donc commencer par lancer le conteneur de la partie frontend pour éviter que l'adresse ne soit utilisée par un autre conteneur.


2) Lancer le back avec :
	```bash
	sudo docker-compose -f docker-compose.yml up --build"
	```


## Usage

3) POST /api/computations :
   Faire un calcul sur la page : "http://172.20.128.2:3001/"
 

4) GET /api/computations/{computation_id} :
   Retrouver le résultat avec l'id de l'opération qui s'incrémente à chaque opération dans : [http://172.20.128.2:3001/results/1](http://172.20.128.2:3001/results/1)
   #tel que 1 est l'id de la première opération effectuée.


5) GET /api/operations/{operation_id} :
   Vérifier l'état de l'opération et le moment de son début et de sa fin grâce à son id dans : [http://172.20.128.2:3001/operations/1](http://172.20.128.2:3001/operations/1)
