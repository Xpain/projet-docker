FROM oraclelinux:7-slim
RUN mkdir -p /data/db
 ADD node-v6* /opt/
 ENV PATH=/opt/node-v6.11.1-linux-x64/bin:$PATH
 ENV NODE_PATH=/opt/node-v6.11.1-linux-x64/lib/node_modules/
 RUN npm install -g amqplib
 RUN npm install -g express
 RUN npm install -g cors
 ADD computations.js .
 CMD [ "node", "computations.js" ]
