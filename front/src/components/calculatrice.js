import { useState } from "react";
import {
    Button,
    Form,
    Select,
} from 'semantic-ui-react'


const options = [
    { key: 'a', text: 'Add', value: 'add' },
    { key: 's', text: 'Substract', value: 'substract' },
    { key: 'm', text: 'Multiply', value: 'multiply' },
    { key: 'd', text: 'Divide', value: 'divide' }
]

export default function () {
    const [formValues, setFormValues] = useState({
        num1: +0,
        num2: +0,
        operation: "add"
    })

    const handleChange = (e, { name, value }) => setFormValues({ ...formValues, [name]: value });
    const handleSubmit = () => {
        const { num1, num2, operation } = formValues;

//        fetch(`http://localhost:3000/api/computations/?num1=${num1}&num2=${num2}&operation=${operation}`, {
//            method: 'POST'
//        })
        fetch('http://localhost:3000/api/computations?' + new URLSearchParams({
    num1,
    num2,
    operation
}),
{
    method:'POST'
})

    }

    return (
        <div className="ui container">
            <h3 className="ui dividing header">Computation Form</h3>
            <Form onSubmit={handleSubmit}>
                <Form.Group widths='equal'>
                    <Form.Input

                        label='num1'
                        name="num1"
                        placeholder='num1'
                        value={formValues.num1}
                        onChange={handleChange}
                    />
                    <Form.Input
                        name="num2"
                        label='num2'
                        placeholder='num2'
                        value={formValues.num2}
                        onChange={handleChange}
                    />
                    <Form.Field
                        control={Select}
                        label='operation'
                        name="operation"
                        value={formValues.operation}
                        onChange={handleChange}
                        options={options}
                        placeholder='operation'
                    />
                    <Button type="submit" content='Compute' />
                </Form.Group>
            </Form>

        </div>
    )
}
