import { useParams } from "react-router-dom";
import { useState } from "react";
import { Item } from 'semantic-ui-react'


export default function () {
    const { id } = useParams();
    const [resultState, setResult] = useState({
        result: {},
        loaded: false
    })
    if (!resultState.loaded) {
        fetch(`http://localhost:3000/api/computations/${id}`, {
        }).then((res) => {
            return res.json()
        }).then(resultResponse => {
            setResult({
                loaded: true,
                result: resultResponse
            })
        })
    }



    return (
        <div className="ui container">
            <h3 className="ui dividing header">Result of operation</h3>
            <Item.Group>
                <Item>
                    <Item.Content>
                        <Item.Header>{`Result : ${resultState.result.result}`}</Item.Header>
                        <Item.Description>
                            <p>Id of operation : {resultState.result.id} </p>
                        </Item.Description>
                    </Item.Content>
                </Item>
            </Item.Group>
        </div>
    )
}
