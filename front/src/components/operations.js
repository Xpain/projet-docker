import { useParams } from "react-router-dom";
import { useState } from "react";
import { Item } from 'semantic-ui-react'


export default function () {
    const { id } = useParams();
    const [operationState, setOperation] = useState({
        operation: {},
        loaded: false
    })
    if (!operationState.loaded) {
        fetch(`http://localhost:3000/api/operations/${id}`, {
        }).then((res) => {
            return res.json()
        }).then(operationResponse => {
            setOperation({
                loaded: true,
                operation: operationResponse
            })
        }).catch(error=>{console.log(error)})
    }



    return (
        <div className="ui container">
            <h3 className="ui dividing header">Operation Status</h3>
            <Item.Group>
                <Item>
                    <Item.Content>
                        <Item.Header>{operationState.operation.status}</Item.Header>
                        <Item.Description>
                            <p>Start Time : {operationState.operation.start} </p>
                            <p>End Time : {operationState.operation.end}</p>
                        </Item.Description>
                    </Item.Content>
                </Item>
            </Item.Group>
        </div>
    )
}
