import Calculatrice from "./components/calculatrice"
import Result from "./components/results.js"
import Operations from "./components/operations.js"
import { Menu } from 'semantic-ui-react'
import {
    BrowserRouter,
    Switch,
    Route,
    Link
} from "react-router-dom";

function App() {
  return (
<BrowserRouter>
        <Menu pointing>
            <Link to="/">
                <Menu.Item
                    name='Compute'
                />
            </Link>
        </Menu>
        <Switch>

            <Route exact path="/">
		<Calculatrice/>
            </Route>
            <Route exact path={`/results/:id/`}>
                <Result />
            </Route >
            <Route exact path={`/operations/:id/`}>
                <Operations />
            </Route>

        </Switch>
    </BrowserRouter>
  );
}

export default App;
