const express = require('express')
const app = express()
var amqp = require('amqplib/callback_api')
var cors = require('cors')


//app.use(cors())

let id = 0

class Operation {
  constructor(num1, num2, operation, timestampstart) {
    id = id + 1
    this.id = id;
    this.num1 = num1;
    this.num2 = num2;
    this.operation = operation;
    this.timestampstart = timestampstart;
    this.timestampend = null;    
    this.status = "Pending";
    
  }
  start(){
      switch(this.operation) {
      case 'add' :
        this.result = parseInt(this.num1) + parseInt(this.num2)
        break
      case 'divide' :
          this.result = parseInt(this.num1) / parseInt(this.num2)
          break
      case 'multiply' :
            this.result = parseInt(this.num1) * parseInt(this.num2)
            break
      case 'subtract' :
        this.result = parseInt(this.num1) - parseInt(this.num2)
        break
    }
    this.status = "Done";
    this.timestampend = new Date();

    
  }
}
let db = []
app.set(db, [])

app.post('/api/computations', cors(), function (req, res) {
	console.log(req.query)
  var op = new Operation(req.query.num1, req.query.num2, req.query.operation, new Date())
  op.start();
  /*Queue sender
  amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'operations';
        var msg = 'Operation started';

        channel.assertQueue(queue, {
            durable: false
        });
        channel.sendToQueue(queue, Buffer.from(msg));

        console.log(" [x] Sent %s", msg);
    });
    setTimeout(function() {
        connection.close();
        process.exit(0);
    }, 500);
  });//*/
  
  console.log()
  let dbb = app.get(db)
  dbb.push(op)
  app.set(db, dbb)
  data = {
    "id" : op.id
  }
  res.status(200).json(data);
})

app.get('/api/computations/:id', cors(), function (req, res) {
  let data = {}
  var id = req.params.id;
  let dbb = app.get(db)
  dbb.forEach(elt => {
    if(elt.id == req.params.id) {
      data = {
        "id" : elt.id,
        "result" : elt.result
      }
    }
  })
  
  res.setHeader('Content-Type', 'application/json');
  res.end ( JSON.stringify(data));
})

app.get('/api/operations/:id', cors(), function (req, res) {
  let data = {}
  var id = req.params.id;
  let dbb = app.get(db)
  dbb.forEach(elt => {
    if(elt.id == req.params.id) {
      data = {
        "start" : elt.timestampstart.toUTCString(),
        "end" : elt.timestampend.toUTCString(),
        "status" : elt.status
      }
    }
    
  })
  // Alternative de lecture avec une queue rabbitmq
      /*Queue receiver
    amqp.connect('amqp://localhost', function(error0, connection) {
	    if (error0) {
		throw error0;
	    }
	    connection.createChannel(function(error1, channel) {
		if (error1) {
		    throw error1;
		}

		var queue = 'operations';

		channel.assertQueue(queue, {
		    durable: false
		});

		console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

		channel.consume(queue, function(msg) {
		    console.log(" [x] Received %s", msg.content.toString());
		    	/*
			dbb.forEach(elt => {
			    if(elt.id == req.params.id) {
			      data = {
				"start" : elt.timestampstart.toUTCString(),
				"end" : elt.timestampend.toUTCString(),
				"status" : msg.content.toString()
			      }
			    }
		}, {
		    noAck: true
		});
	    });
	});*/
	  res.setHeader('Content-Type', 'application/json');
 	  res.end ( JSON.stringify(data));

})

app.listen(3000, function () {
  console.log('Addition service listening on port 3000!')
})


